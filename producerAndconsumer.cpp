#include <pthread.h>
#include <queue>
#include <string>
#include <iostream>


const int maxNumber = 20;

class ProducerAndConsumer
{
public:
    ProducerAndConsumer()
    {
        pthread_mutex_init(&the_mutex, nullptr);
        pthread_cond_init(&condc, nullptr);
        pthread_cond_init(&condp, nullptr);
        pthread_create(&con, nullptr, producer, nullptr);
        pthread_create(&pro, nullptr, consumer, nullptr);

    }
    ~ProducerAndConsumer()
    {
        pthread_mutex_destroy(&the_mutex);
        pthread_cond_destroy(&condc);
        pthread_cond_destroy(&condp);
    }

    static void* producer(void* ptr)
    {
        int i = 0;
        while(i++ < maxNumber)
        {
            std::string input = std::to_string(i);
            pthread_mutex_lock(&the_mutex);
            while (buffer.size() >= maxNumber) pthread_cond_wait(&condp, &the_mutex);
            std::cout << "producer" << i << std::endl;
            buffer.push(input);
            pthread_cond_signal(&condc);
            pthread_mutex_unlock(&the_mutex);
        }
        pthread_exit(0);
    }

    static void* consumer(void* ptr)
    {
        int j = maxNumber;
        while(j--)
        {
            pthread_mutex_lock(&the_mutex);
            while(buffer.empty()) pthread_cond_wait(&condc, &the_mutex);
            std::cout << "consumer :" << buffer.front() << std::endl;
            buffer.pop();
            pthread_cond_signal(&condp);
            pthread_mutex_unlock(&the_mutex);
        }
        pthread_exit(0);
    }

    void start()
    {
        pthread_join(pro, nullptr);
        pthread_join(con, nullptr);
    }

private:
    static pthread_mutex_t the_mutex;
    static pthread_cond_t condc, condp;
    static std::queue<std::string> buffer;
    pthread_t pro, con;
};

pthread_mutex_t ProducerAndConsumer::the_mutex;
pthread_cond_t ProducerAndConsumer::condc;
pthread_cond_t ProducerAndConsumer::condp;
std::queue<std::string> ProducerAndConsumer::buffer;

class OutPutInTurn
{
public:
    OutPutInTurn()
    {
        pthread_mutex_init(&the_mutex, nullptr);
        pthread_cond_init(&condA, nullptr);
        pthread_cond_init(&condB, nullptr);
        pthread_cond_init(&condC, nullptr);
        pthread_create(&threadA, nullptr, threadAProcess, nullptr);
        pthread_create(&threadB, nullptr, threadBProcess, nullptr);
        pthread_create(&threadC, nullptr, threadCProcess, nullptr);

    }

    ~OutPutInTurn()
    {
        pthread_mutex_destroy(&the_mutex);
        pthread_cond_destroy(&condA);
        pthread_cond_destroy(&condB);
        pthread_cond_destroy(&condC);
    }

    static void* threadAProcess(void* attr)
    {
        int count = 10;
        while(count--)
        {
            pthread_mutex_lock(&the_mutex);
            while(flag != 0) pthread_cond_wait(&condA, &the_mutex);
            flag = 1;

            pthread_t         self;
            self = pthread_self();
            std::cout << flag - 1;

            pthread_cond_signal(&condB);
            pthread_mutex_unlock(&the_mutex);
        }
        pthread_exit(nullptr);
    }

    static void* threadBProcess(void* attr)
    {
        int count = 10;
        while(count--)
        {
            pthread_mutex_lock(&the_mutex);
            while(flag != 1) pthread_cond_wait(&condB, &the_mutex);
            flag = 2;

            pthread_t         self;
            self = pthread_self();
            std::cout << flag - 1;

            pthread_cond_signal(&condC);
            pthread_mutex_unlock(&the_mutex);
        }
        pthread_exit(nullptr);
    }

    static void* threadCProcess(void* attr)
    {
        int count = 10;
        while(count--)
        {
            pthread_mutex_lock(&the_mutex);
            while(flag != 2) pthread_cond_wait(&condC, &the_mutex);
            flag = 0;

            pthread_t         self;
            self = pthread_self();
            std::cout << 2 << std::endl;

            pthread_cond_signal(&condA);
            pthread_mutex_unlock(&the_mutex);
        }
        pthread_exit(nullptr);
    }

    void join()
    {
        pthread_join(threadA, nullptr);
        pthread_join(threadB, nullptr);
        pthread_join(threadC, nullptr);
    }

private:
    static pthread_mutex_t the_mutex;
    static pthread_cond_t condA, condB, condC;
    static int flag;
    pthread_t threadA, threadB, threadC;
};

pthread_mutex_t OutPutInTurn::the_mutex;
pthread_cond_t OutPutInTurn::condA;
pthread_cond_t OutPutInTurn::condB;
pthread_cond_t OutPutInTurn::condC;
int OutPutInTurn::flag = 0;

int main()
{
//    ProducerAndConsumer proAndCon;
  //  proAndCon.start();

    OutPutInTurn output;
    output.join();
    return 0;
}
